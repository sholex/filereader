No tests, almost no exceptions. Just POC for handling different file types.

# Project start:
1. from the src folder `composer dump-autoload`
2. from the application root directory run `docker-compose up`

After that you will be able to access project on the http://localhost:8080

Example of usage you can find in `src/public/views/table.php`

