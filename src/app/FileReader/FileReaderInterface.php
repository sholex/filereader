<?php
declare(strict_types=1);

namespace App\FileReader;

interface FileReaderInterface
{
    public function parseData(string $fileName): self;
    public function getData(): array;
}