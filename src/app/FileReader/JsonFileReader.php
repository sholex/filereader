<?php
declare(strict_types=1);

namespace App\FileReader;

class JsonFileReader extends FileReader
{
    public function parseData(string $fileName): self
    {
        $this->data = json_decode(file_get_contents($fileName), true);

        return $this;
    }
}