<?php
declare(strict_types=1);

namespace App\FileReader;

class XmlFileReader extends FileReader
{
    private const ELEMENT_PATH = 'element';
    public function parseData(string $fileName): self
    {
        $this->data = json_decode(json_encode(simplexml_load_file($fileName)), true)[self::ELEMENT_PATH];

        return $this;
    }
}