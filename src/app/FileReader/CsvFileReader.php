<?php
declare(strict_types=1);

namespace App\FileReader;

use \SplFileObject;

class CsvFileReader extends FileReader
{
    public function parseData(string $fileName): self
    {
        $file = new SplFileObject($fileName, 'r+');

        while (!$file->eof()) {
            $lineData = $file->fgetcsv();
            if (!empty(array_filter($lineData))) {
                $this->data[] = $lineData;
            }
        }

        $this->data = $this->formatRawData();

        return $this;
    }

    private function formatRawData(): array
    {
        $keys = array_shift($this->data);

        $formattedData = [];
        foreach ($this->data as $item) {
            $row = [];
            foreach ($keys as $i => $key) {
                $row[$key] = $item[$i] ?? null;
            }
            $formattedData[] = $row;
        }

        return $formattedData;
    }
}