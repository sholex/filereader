<?php
declare(strict_types=1);

namespace App\FileReader;

class FileReaderFactory
{
    private const CSV_FILE = 'text/csv';
    private const JSON_FILE = 'application/json';
    private const XML_FILE = 'text/xml';

    private const ERROR_MESSAGE = 'Unsupported file type';

    public static function getFileReader(string $fileType): FileReaderInterface
    {
        switch ($fileType) {
            case self::CSV_FILE:
                return new CsvFileReader();
            case self::JSON_FILE:
                return new JsonFileReader();
            case self::XML_FILE:
                return new XmlFileReader();
            default:
                throw new \Exception(self::ERROR_MESSAGE);
        }
    }
}