<?php
declare(strict_types=1);

namespace App\FileReader;

abstract class FileReader implements FileReaderInterface
{
    protected array $data = [];

    public function getData(): array
    {
        return $this->data;
    }
}