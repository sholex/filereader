<?php
$fileName = $_FILES['file']['tmp_name'] ?? null;
$fileType = $_FILES['file']['type'] ?? null;

if (empty($fileType) || empty($fileName)) {
    return;
}

$fields = ['login', 'firstname', 'lastname', 'email'];

$data = App\FileReader\FileReaderFactory::getFileReader($fileType)
    ->parseData($fileName)
    ->getData();

$rowNum = 1;
?>

<div class="col-md-6">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Table</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th>#</th>
                    <?php foreach ($fields as $field): ?>
                        <th><?= $field ?></th>
                    <?php endforeach; ?>
                </tr>
                <?php foreach ($data as $item): ?>
                    <tr>
                        <td><?= $rowNum++ ?></td>
                        <?php foreach ($fields as $field): ?>
                            <td><?= $item[$field] ?></td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>